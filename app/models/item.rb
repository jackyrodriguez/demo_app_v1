class Item < ApplicationRecord
    validates :name, presence: { message: "Name can't be blank" }
end
