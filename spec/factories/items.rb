FactoryBot.define do
    factory :item do
      name { "Sample Item" }
      description { "This is a sample item description" }
    end
  end