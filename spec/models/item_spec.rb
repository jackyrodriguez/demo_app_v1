# spec/models/item_spec.rb
require 'rails_helper'

RSpec.describe Item, type: :model do
  context 'with valid attributes' do
    it "is valid" do
      item = build(:item, name: "Example", description: "This is an example description")
      expect(item).to be_valid
    end
  end

  context 'with invalid attributes' do
    it "is not valid without a name" do
      item = build(:item, name: nil, description: "This is an example description")
      item.valid?
      expect(item.errors[:name]).to include("Name can't be blank")
    end
  end
end
